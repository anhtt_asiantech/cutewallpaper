package com.anhttvn.cutewallpaper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.anhttvn.cutewallpaper.R;
import com.anhttvn.cutewallpaper.model.Wallpaper;
import com.squareup.picasso.Picasso;

import java.util.List;


public class HomeAdapter extends PagerAdapter implements View.OnClickListener {

  private Context mContext;
  private List<Wallpaper> wallpapers;
  private SlideNewsClick slideNewsClick;
  public HomeAdapter(Context context, List<Wallpaper> list, SlideNewsClick click) {
    this.mContext = context;
    this.wallpapers = list;
    this.slideNewsClick = click;
  }
  @Override
  public int getCount() {
    return wallpapers.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object o) {
    return view == o;
  }

  @Override
  public  Object instantiateItem(ViewGroup container, int position) {
    View view = LayoutInflater.from(mContext).inflate(R.layout.home_item, container, false);
    ImageView image = view.findViewById(R.id.imgNews);
    TextView title = view.findViewById(R.id.title);
    CardView cardNews = view.findViewById(R.id.cardNews);
    title.setText(wallpapers.get(position).getTitle());
    if (wallpapers.get(position).getPath() == null || wallpapers.get(position).getPath().isEmpty()) {
      image.setImageResource(R.drawable.ic_no_thumbnail);
    } else {
      Picasso.with(mContext).load(wallpapers.get(position).getPath())
              .placeholder(R.drawable.ic_no_thumbnail)
              .error(R.drawable.ic_no_thumbnail)
              .into(image);
    }

    cardNews.setTag(position);
    cardNews.setOnClickListener(this);

    container.addView(view);



    return view;
  }


  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((View) object);
  }

  @Override
  public int getItemPosition(Object object) {
    return super.getItemPosition(object);
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()) {
      case R.id.cardNews:
        slideNewsClick.detailNews(position);
        break;
      default:
        break;
    }
  }

  public interface SlideNewsClick {
    void detailNews(int position);
  }
}
